//package com.example;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

public class ConverterTest {

    CalculatorImplementation calculator = new CalculatorImplementation();
    Calculator calculatorMock;
    Converter converter;

    @Before
    public void setup() {
        calculator.multiply(1, 0.453592);
        calculator.multiply(2, 0.453592);
        calculator.multiply(3, 0.453592);
        calculator.multiply(4, 0.453592);
        calculator.multiply(10, 0.453592);
        calculatorMock = new CalculatorMock(calculator.getInvocationHistory());
        converter = new Converter(calculatorMock);
    }

    @Test
    public void testForOnePound() {
        double result = converter.convertPoundsToKg(1);
        assertThat(result).isEqualTo(0.453592);
    }

    @Test
    public void testForTwoPounds() {
        double result = converter.convertPoundsToKg(1);
        assertThat(result).isEqualTo(0.453592);
    }

    @Test
    public void testForTenPounds() {
        double result = converter.convertPoundsToKg(1);
        assertThat(result).isEqualTo(0.453592);
    }

}

