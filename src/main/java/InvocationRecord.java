public class InvocationRecord {
    double input1;
    double input2;
    double output;

    public InvocationRecord(double input1, double input2, double output) {
        this.input1 = input1;
        this.input2 = input2;
        this.output = output;
    }
}
