import java.util.List;

public class CalculatorMock implements Calculator {

    private List<InvocationRecord> invocationRecords;

    public CalculatorMock(List<InvocationRecord> invocationRecords) {
        this.invocationRecords = invocationRecords;
    }

    @Override
    public double multiply(double x, double y) {
        return invocationRecords.stream().filter(e -> e.input1 == x && e.input2 ==y).findFirst().get().output;
    }
}
