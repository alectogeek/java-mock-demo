import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class CalculatorImplementation implements Calculator {

    private Gson gson = new Gson();
    private List<InvocationRecord> invocationRecords;

    public CalculatorImplementation() {
        invocationRecords = new ArrayList<InvocationRecord>();
    }

    public double multiply(double x, double y) {
        final double result = x * y;

        invocationRecords.add(new InvocationRecord(x, y, result));
//        try {
//            String jsonString = gson.toJson(invocationRecord);
//            FileWriter myWriter = new FileWriter("invocation_records.json", true);
//            myWriter.write(jsonString + "\n");
//            myWriter.close();

//            System.out.println(jsonString);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return result;
    }

    List<InvocationRecord> getInvocationHistory() {
        return this.invocationRecords;
    }

}
