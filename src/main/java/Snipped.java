public class Snipped {

    public static void main(String[] args) {
        Calculator calculator = new CalculatorImplementation();
        Converter converter = new Converter(calculator);

        System.out.println(converter.convertPoundsToKg(2));
    }
}
