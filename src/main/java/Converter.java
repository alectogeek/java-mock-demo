public class Converter {
    Calculator calculator;

    public Converter(Calculator calculator) {
        this.calculator = calculator;
    }

    public double convertPoundsToKg(double input) {
        return calculator.multiply(input, 0.453592);
    }
}
